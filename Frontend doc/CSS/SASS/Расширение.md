Расширение или наследование в SASS позволяет одному селектору унаследовать все стили другого селектора, что упрощает повторное использование кода и поддержание единообразия стилей. Вот несколько реальных примеров, демонстрирующих мощь и гибкость этой функции.

### Пример 1: Создание вариантов кнопок

```scss
// Базовый стиль кнопки
.btn {
  padding: 10px 20px;
  border: 1px solid transparent;
  border-radius: 4px;
  font-weight: bold;
  text-align: center;
  cursor: pointer;
  display: inline-block;
}

// Вариант для основной кнопки
.btn-primary {
  @extend .btn;
  background-color: #007bff;
  color: white;
}

// Вариант для кнопки предупреждения
.btn-warning {
  @extend .btn;
  background-color: #ffc107;
  color: black;
}
```

### Пример 2: Создание утилитных классов

```scss
// Базовый утилитный класс для текста
.text-center {
  text-align: center;
}

// Расширение для специфического использования
.banner-text {
  @extend .text-center;
  font-size: 24px;
  font-weight: bold;
}
```

### Пример 3: Создание системы сетки

```scss
// Базовый класс для колонок
.col {
  float: left;
  padding: 15px;
}

// Классы для определения ширины колонок
.col-1 { @extend .col; width: 8.33%; }
.col-2 { @extend .col; width: 16.66%; }
.col-3 { @extend .col; width: 25%; }
.col-4 { @extend .col; width: 33.33%; }
.col-5 { @extend .col; width: 41.66%; }
.col-6 { @extend .col; width: 50%; }
.col-7 { @extend .col; width: 58.33%; }
.col-8 { @extend .col; width: 66.66%; }
.col-9 { @extend .col; width: 75%; }
.col-10 { @extend .col; width: 83.33%; }
.col-11 { @extend .col; width: 91.66%; }
.col-12 { @extend .col; width: 100%; }
```

### Пример 4: Создание тематических кнопок с общими стилями

```scss
// Базовый стиль для кнопок
.button {
  padding: 0.5em 1em;
  border: none;
  border-radius: 5px;
  font-size: 16px;
  cursor: pointer;
}

// Расширение для создания тематических вариантов
.success-button {
  @extend .button;
  background-color: #4CAF50;
  color: white;
}

.error-button {
  @extend .button;
  background-color: #F44336;
  color: white;
}

.info-button {
  @extend .button;
  background-color: #2196F3;
  color: white;
}
```

Эти примеры демонстрируют, как расширение/наследование в SASS может упростить создание консистентных и легко поддерживаемых стилей, минимизируя дублирование кода и улучшая организацию стилевых файлов.