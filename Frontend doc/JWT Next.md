JWT (JSON Web Token) авторизация - это метод авторизации, при котором сервер генерирует токен, который клиент сохраняет и отправляет обратно на сервер при каждом запросе.

NextAuth.js - это библиотека для Next.js, которая облегчает процесс создания защищенных аутентификационных маршрутов.

Axios - это популярная библиотека JavaScript, которую можно использовать для отправки HTTP-запросов.

Ниже приведен пример использования этих технологий для создания JWT авторизации на Next.js.

Установите необходимые библиотеки:

```bash
npm install next-auth axios
```

Создайте файл `[...nextauth].js` в директории `pages/api/auth` вашего проекта и добавьте следующий код:

```jsx
import NextAuth from 'next-auth'
import Providers from 'next-auth/providers'

export default NextAuth({
  providers: [
    Providers.Credentials({
      name: 'Credentials',
      credentials: {
        username: { label: "Username", type: "text" },
        password: {  label: "Password", type: "password" }
      },
      authorize: async (credentials) => {
        const axios = require('axios');
        const res = await axios.post('https://your-authentication-api.com', {
          username: credentials.username,
          password: credentials.password
        });
        const user = res.data;

        if (user) {
          return Promise.resolve(user)
        } else {
          return Promise.resolve(null)
        }
      }
    })
  ],
  session: {
    jwt: true,
  },
  callbacks: {
    async jwt(token, user) {
      if (user) {
        token.id = user.id;
      }
      return token;
    },
    async session(session, token) {
      session.userId = token.id;
      return session;
    },
  },
})
```

В этом коде мы используем провайдер `Credentials` от `next-auth/providers` для создания формы входа с полями для имени пользователя и пароля. В функции `authorize` мы используем `axios` для отправки этих данных на наш аутентификационный API. Если пользователь успешно аутентифицирован, мы возвращаем его данные, которые затем используются для создания JWT.

В секции `callbacks` мы определяем, какие данные должны быть включены в JWT и сессию. В этом примере мы просто включаем ID пользователя.

Теперь вы можете использовать хуки `useSession` и `signIn` от `next-auth/client` для входа пользователя и получения информации о сессии:

```jsx
import { signIn, useSession } from 'next-auth/client'

export default function Component() {
  const [ session, loading ] = useSession()

  return <>
    {!session && <>
      Not signed in <br/>
      <button onClick={() => signIn('credentials', { callbackUrl: 'http://localhost:3000/your-redirect-url' })}>Sign in</button>
    </>}
    {session && <>
      Signed in as {session.user.email} <br/>
      <button onClick={() => signOut()}>Sign out</button>
    </>}
  </>
}
```

Помните, что вам необходимо заменить 'https://your-authentication-api.com' и 'http://localhost:3000/your-redirect-url' на реальные URL-адреса вашего аутентификационного API и URL-адреса перенаправления после входа в систему соответственно.