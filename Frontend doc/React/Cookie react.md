Использование cookies в React - это простой процесс, который можно осуществить с помощью различных библиотек, таких как js-cookie или universal-cookie. 

Пример использования с помощью библиотеки js-cookie:

```javascript
import Cookies from 'js-cookie'

// Создание cookie
Cookies.set('name', 'value')

// Чтение cookie
Cookies.get('name') // => 'value'

// Удаление cookie
Cookies.remove('name')
```

Если вы хотите установить срок действия cookie, вы можете сделать это, передав второй аргумент в метод set:

```javascript
// Создание cookie, которое истекает через 7 дней
Cookies.set('name', 'value', { expires: 7 })
```

Также можно установить дополнительные атрибуты для cookie:

```javascript
Cookies.set('name', 'value', { secure: true, sameSite: 'Strict' })
```

Если вы хотите использовать библиотеку universal-cookie, вот пример:

```javascript
import Cookies from 'universal-cookie';

const cookies = new Cookies();

// Создание cookie
cookies.set('name', 'value', { path: '/' });

// Чтение cookie
cookies.get('name'); // => 'value'

// Удаление cookie
cookies.remove('name');
```

Важно помнить, что вам следует избегать хранения чувствительной информации в cookies из-за возможных проблем с безопасностью.