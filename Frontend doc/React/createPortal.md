`createPortal` — это метод из библиотеки React, который позволяет рендерить дочерние компоненты в DOM-узле, который находится вне текущего компонента DOM-иерархии родителя. Это особенно полезно для создания компонентов, которые должны "вырываться" из текущего контекста, например, модальные окна, всплывающие подсказки и нотификации, которые должны быть размещены в другом месте DOM-дерева, чтобы правильно функционировать или избежать проблем со стилями.

### Основные особенности `createPortal`:

- **Изоляция событий:** События, сгенерированные внутри портала, ведут себя так, как будто компонент находится в исходном DOM-дереве, сохраняя при этом естественный поток событий в React.
- **Доступ к контексту:** Компоненты внутри портала имеют доступ к тому же контексту, что и компоненты в основном дереве, несмотря на то, что они физически находятся в другом месте DOM.

### Пример использования `createPortal`:

Допустим, у нас есть модальное окно, которое мы хотим рендерить напрямую в `body` документа, чтобы избежать проблем с переполнением и позиционированием, которые могут возникнуть, если модальное окно будет находиться внутри глубоко вложенного компонента.

Сначала создадим контейнер для портала в `public/index.html`:

```html
<div id="modal-root"></div>
```

Теперь реализуем компонент `Modal` с использованием `createPortal`:

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

const modalRoot = document.getElementById('modal-root');

class Modal extends React.Component {
  constructor(props) {
    super(props);
    this.el = document.createElement('div');
  }

  componentDidMount() {
    modalRoot.appendChild(this.el);
  }

  componentWillUnmount() {
    modalRoot.removeChild(this.el);
  }

  render() {
    return ReactDOM.createPortal(
      this.props.children,
      this.el
    );
  }
}

export default Modal;
```

Использование компонента `Modal`:

```jsx
import React, { useState } from 'react';
import Modal from './Modal';

function App() {
  const [isOpen, setIsOpen] = useState(false);

  return (
    <div>
      <button onClick={() => setIsOpen(true)}>Открыть модальное окно</button>
      {isOpen && (
        <Modal>
          <div style={{ backgroundColor: 'white', padding: '20px' }}>
            <p>Это контент модального окна</p>
            <button onClick={() => setIsOpen(false)}>Закрыть</button>
          </div>
        </Modal>
      )}
    </div>
  );
}

export default App;
```

В этом примере, когда пользователь нажимает кнопку "Открыть модальное окно", модальное окно рендерится в элементе с `id="modal-root"`, который находится вне основного приложения React. Это позволяет модальному окну быть на верхнем уровне документа, избегая проблем с CSS и позиционированием, которые могут возникнуть, если бы оно рендерилось внутри глубоко вложенного компонента.

Давайте рассмотрим пример использования `createPortal` в функциональном компоненте React. В этом примере мы создадим модальное окно, которое будет рендериться в элементе с `id="modal-root"`, находящемся вне основного дерева приложения.

Сначала добавим контейнер для нашего модального окна в `public/index.html` файла вашего React приложения:

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>React App</title>
</head>
<body>
  <div id="root"></div>
  <div id="modal-root"></div>
</body>
</html>
```

Теперь создадим функциональный компонент `Modal`, который будет использовать `createPortal` для рендеринга своего содержимого в `modal-root`:

```jsx
import React from 'react';
import ReactDOM from 'react-dom';

const Modal = ({ children }) => {
  // Элемент, в который будет рендериться модальное окно
  const modalRoot = document.getElementById('modal-root');
  
  // Используем createPortal для рендеринга дочерних элементов в modal-root
  return ReactDOM.createPortal(
    // Модальное окно и его содержимое
    <div style={{ position: 'fixed', top: '20%', left: '50%', transform: 'translate(-50%, -50%)', backgroundColor: 'white', padding: '20px', zIndex: 1000 }}>
      {children}
    </div>,
    modalRoot
  );
};

export default Modal;
```

И наконец, используем наш компонент `Modal` в приложении:

```jsx
import React, { useState } from 'react';
import Modal from './Modal';

const App = () => {
  const [isModalOpen, setIsModalOpen] = useState(false);

  return (
    <div>
      <button onClick={() => setIsModalOpen(true)}>Открыть модальное окно</button>
      {isModalOpen && (
        <Modal>
          <div>
            <p>Это контент модального окна</p>
            <button onClick={() => setIsModalOpen(false)}>Закрыть</button>
          </div>
        </Modal>
      )}
    </div>
  );
};

export default App;
```

В этом примере, когда пользователь нажимает кнопку "Открыть модальное окно", модальное окно отображается на экране. Модальное окно рендерится в элементе с `id="modal-root"`, который находится вне основного дерева приложения (`#root`). Это позволяет избежать потенциальных проблем с CSS и обеспечивает, что модальное окно будет отображаться поверх других элементов интерфейса. Кнопка "Закрыть" в модальном окне изменяет состояние `isModalOpen` на `false`, что приводит к его закрытию.