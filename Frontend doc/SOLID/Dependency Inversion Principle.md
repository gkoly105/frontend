Принцип инверсии зависимостей (Dependency Inversion Principle, DIP) является одним из пяти основных принципов SOLID в объектно-ориентированном программировании. Этот принцип состоит из двух основных идей:

1. Модули высокого уровня не должны зависеть от модулей низкого уровня. Оба типа модулей должны зависеть от абстракций.
2. Абстракции не должны зависеть от деталей. Детали должны зависеть от абстракций.

Целью DIP является уменьшение зависимостей между компонентами программы, что делает систему более гибкой и упрощает её модификацию и тестирование.

### Пример без применения DIP:

Допустим, у нас есть приложение, которое читает данные из файла и затем обрабатывает их.

```javascript
class FileReader {
    read() {
        // Чтение данных из файла
        return "Данные из файла";
    }
}

class DataProcessor {
    constructor(fileReader) {
        this.fileReader = fileReader;
    }

    process() {
        const data = this.fileReader.read();
        console.log(`Обработка данных: ${data}`);
    }
}

const fileReader = new FileReader();
const dataProcessor = new DataProcessor(fileReader);
dataProcessor.process();
```

В этом примере `DataProcessor` напрямую зависит от конкретной реализации `FileReader`. Это означает, что если мы захотим изменить способ получения данных (например, читать их из сети вместо файла), нам придется изменить код класса `DataProcessor`.

### Пример с применением DIP:

Чтобы следовать принципу инверсии зависимостей, мы можем определить абстракцию (интерфейс) для чтения данных, и затем зависеть от этой абстракции, а не от конкретной реализации.

```javascript
// Абстракция, от которой будут зависеть модули высокого уровня
class IDataReader {
    read() {
        throw new Error("read() method must be implemented");
    }
}

// Конкретная реализация для чтения данных из файла
class FileReader extends IDataReader {
    read() {
        // Чтение данных из файла
        return "Данные из файла";
    }
}

// Конкретная реализация для чтения данных из сети
class NetworkReader extends IDataReader {
    read() {
        // Чтение данных из сети
        return "Данные из сети";
    }
}

class DataProcessor {
    constructor(dataReader) {
        this.dataReader = dataReader;
    }

    process() {
        const data = this.dataReader.read();
        console.log(`Обработка данных: ${data}`);
    }
}

// Теперь мы можем легко заменить способ получения данных, не изменяя класс DataProcessor
const fileReader = new FileReader();
const dataProcessor1 = new DataProcessor(fileReader);
dataProcessor1.process();

const networkReader = new NetworkReader();
const dataProcessor2 = new DataProcessor(networkReader);
dataProcessor2.process();
```

В этом примере `DataProcessor` зависит от абстракции `IDataReader`, а не от конкретных реализаций `FileReader` или `NetworkReader`. Это позволяет легко изменять способ получения данных, подставляя различные реализации `IDataReader`, без необходимости изменения кода `DataProcessor`.

Применение принципа инверсии зависимостей делает систему более гибкой и устойчивой к изменениям, упрощая добавление новых функций и тестирование комп