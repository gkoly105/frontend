Принцип подстановки Барбары Лисков (Liskov Substitution Principle, LSP) является одним из пяти основных принципов SOLID в объектно-ориентированном программировании. Этот принцип утверждает, что объекты базового класса должны быть заменяемы объектами их подклассов без нарушения работы программы. Другими словами, подклассы должны быть взаимозаменяемы с их базовыми классами, при этом не изменяя корректность программы.

### Пример без применения LSP:

Рассмотрим следующий пример, где у нас есть класс `Rectangle` и его подкласс `Square`.

```javascript
class Rectangle {
    constructor(width, height) {
        this._width = width;
        this._height = height;
    }

    set width(value) {
        this._width = value;
    }

    get width() {
        return this._width;
    }

    set height(value) {
        this._height = value;
    }

    get height() {
        return this._height;
    }

    get area() {
        return this._width * this._height;
    }
}

class Square extends Rectangle {
    set width(value) {
        this._width = value;
        this._height = value;
    }

    set height(value) {
        this._width = value;
        this._height = value;
    }
}

function increaseRectangleWidth(rectangle) {
    rectangle.width = rectangle.width + 1;
}

const rectangle = new Rectangle(10, 2);
increaseRectangleWidth(rectangle);
console.log(rectangle.area); // 22

const square = new Square(5, 5);
increaseRectangleWidth(square);
console.log(square.area); // 36, но ожидалось 30
```

В этом примере, `Square` наследуется от `Rectangle`, и мы могли бы ожидать, что `Square` может быть использован вместо `Rectangle`. Однако, из-за того, что квадрат имеет равные стороны, изменение ширины также изменяет высоту, что приводит к непредвиденным результатам при использовании функции `increaseRectangleWidth`. Это нарушает принцип подстановки Лисков, так как `Square` не может быть использован везде, где ожидается `Rectangle`.

### Пример с применением LSP:

Чтобы следовать принципу подстановки Лисков, мы должны переосмыслить нашу иерархию классов так, чтобы подклассы могли бы быть использованы вместо базовых классов без изменения ожидаемого поведения.

```javascript
class Shape {
    get area() {
        throw new Error("Area method must be implemented");
    }
}

class Rectangle extends Shape {
    constructor(width, height) {
        super();
        this._width = width;
        this._height = height;
    }

    get area() {
        return this._width * this._height;
    }
}

class Square extends Shape {
    constructor(side) {
        super();
        this._side = side;
    }

    get area() {
        return this._side * this._side;
    }
}

function printArea(shape) {
    console.log(shape.area);
}

const rectangle = new Rectangle(10, 5);
const square = new Square(5);

printArea(rectangle); // 50
printArea(square); // 25
```

В этом примере, и `Rectangle`, и `Square` наследуются от абстрактного класса `Shape`, который определяет общий интерфейс для расчета площади. Теперь оба класса могут быть использованы взаимозаменяемо там, где ожидается `Shape`, без нарушения ожидаемого поведения программы. Это соответствует принципу подстановки Лисков, так как подклассы могут служить заменой для их базовых классов