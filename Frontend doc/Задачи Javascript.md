Задача 1: Создайте функцию, которая принимает строку и возвращает эту же строку, но с обратным порядком символов.

Решение:

```javascript
function reverseString(str) {
    return str.split('').reverse().join('');
}
console.log(reverseString('Hello')); // 'olleH'
```

Задача 2: Напишите функцию, которая принимает массив и возвращает новый массив, содержащий только уникальные элементы из исходного массива.

Решение:

```javascript
function uniqueElements(arr) {
    return [...new Set(arr)];
}
console.log(uniqueElements([1, 2, 2, 3, 3, 4, 5, 5, 6])); // [1, 2, 3, 4, 5, 6]
```

Задача 3: Создайте функцию, которая принимает два числа и возвращает их сумму.

Решение:

```javascript
function addNumbers(num1, num2) {
    return num1 + num2;
}
console.log(addNumbers(5, 7)); // 12
```

Задача 4: Напишите функцию, которая принимает строку и возвращает количество гласных в этой строке.

Решение:

```javascript
function countVowels(str) {
    let count = 0;
    const vowels = ['a', 'e', 'i', 'o', 'u'];
    for(let char of str.toLowerCase()) {
        if(vowels.includes(char)) {
            count++;
        }
    }
    return count;
}
console.log(countVowels('Hello World')); // 3
```

Задача 5: Создайте функцию, которая принимает массив чисел и возвращает самое большое число в массиве.

Решение:

```javascript
function maxNumber(arr) {
    return Math.max(...arr);
}
console.log(maxNumber([1, 2, 3, 4, 5])); // 5
```